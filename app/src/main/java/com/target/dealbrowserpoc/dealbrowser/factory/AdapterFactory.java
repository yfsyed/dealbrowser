package com.target.dealbrowserpoc.dealbrowser.factory;

import android.databinding.ViewDataBinding;

import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.adapter.DealsAdapter;
import com.target.dealbrowserpoc.dealbrowser.adapter.RecyclerViewAdapterImpl;

public class AdapterFactory {

    public enum Type {
        DEALS_GRID(R.layout.deal_grid_item),
        DEALS_LIST(R.layout.deal_list_item);

        private int layoutId;

        public int getLayoutId() {
            return layoutId;
        }

        Type(int layoutId) {
            this.layoutId = layoutId;
        }
    }

    public static <T extends ViewDataBinding> DealsAdapter<T> getAdapterInstance(Type type, Object... data) {
        int layoutId = type.getLayoutId();
        switch (type) {
            case DEALS_LIST:
                return new RecyclerViewAdapterImpl.DealsListAdapter<T>(layoutId, data[0], data[1]);

            case DEALS_GRID:
                return new RecyclerViewAdapterImpl.DealsGridAdapter<T>(layoutId, data[0], data[1], data[2]);
        }
        return null;
    }
}
