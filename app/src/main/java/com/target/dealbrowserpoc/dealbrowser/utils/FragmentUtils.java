package com.target.dealbrowserpoc.dealbrowser.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.target.dealbrowserpoc.dealbrowser.factory.FragmentFactory;

import java.lang.ref.WeakReference;

public class FragmentUtils {

    public static void showFragmentWithType(WeakReference<Context> ctx, FragmentFactory.Type type,
                                            int containerId, boolean backstack) {
        if (type == null || ctx.get() == null) return;

        try {
            FragmentManager fm = getFragmentManager(ctx.get());
            Fragment fragment = fm.findFragmentByTag(type.getFragmentName());
            if (fragment == null) {
                fragment = FragmentFactory.getFragmentClass(type);
            }
            addFragment(fm, containerId, fragment, type.getFragmentName(), backstack);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    private static void addFragment(FragmentManager fm, int containerId,
                                    Fragment fragment, String name, boolean backstack) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(containerId, fragment);
        if (backstack) {
            ft.addToBackStack(name);
        }
        ft.commitAllowingStateLoss();
    }

    private static FragmentManager getFragmentManager(Context ctx) {
        return ((AppCompatActivity) ctx).getSupportFragmentManager();
    }

    public static int getCount(FragmentManager fm) {
        return fm.getBackStackEntryCount();
    }
}
