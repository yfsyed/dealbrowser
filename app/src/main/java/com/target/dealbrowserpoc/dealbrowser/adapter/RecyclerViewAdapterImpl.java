package com.target.dealbrowserpoc.dealbrowser.adapter;

import android.databinding.ViewDataBinding;

import com.target.dealbrowserpoc.dealbrowser.model.DealItem;
import com.target.dealbrowserpoc.dealbrowser.viewModel.DealsBrowserVM;

import java.util.List;

public class RecyclerViewAdapterImpl {

    /**
     * Show Deal list adapter for deals.
     */
    public static class DealsListAdapter<T extends ViewDataBinding> extends
            AbstractRecyclerAdapter<T, DealsBrowserVM, DealItem> {

        public DealsListAdapter(int layoutId, Object callbackVM, Object doorList) {
            super(layoutId, (DealsBrowserVM) callbackVM, (List<DealItem>) doorList);
        }
    }

    /**
     * Show Deal Grid adapter for deals.
     */
    public static class DealsGridAdapter<T extends ViewDataBinding> extends
            AbstractRecyclerAdapter<T, DealsBrowserVM, DealItem> {

        public DealsGridAdapter(int layoutId, Object callbackVM, Object doorList, Object widthPx) {
            super(layoutId, (DealsBrowserVM) callbackVM, (List<DealItem>) doorList, (int) widthPx);
        }
    }
}
