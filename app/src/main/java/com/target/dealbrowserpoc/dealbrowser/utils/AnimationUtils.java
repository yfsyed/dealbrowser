package com.target.dealbrowserpoc.dealbrowser.utils;

import android.content.Context;
import android.support.transition.Fade;
import android.support.transition.Transition;
import android.support.transition.TransitionInflater;
import android.support.transition.TransitionSet;

public class AnimationUtils {

    private static final int MOVE_DEFAULT_TIME = 200;
    private static final int FADE_DEFAULT_TIME = 100;

    // 1. Exit Transition for Old Fragment.
    public static Fade getExitAnimation() {
        Fade exitFade = new Fade();
        exitFade.setDuration(FADE_DEFAULT_TIME);
        return exitFade;
    }

    // 2. Shared Elements Transition
    public static TransitionSet getTransition(Context ctx) {
        TransitionSet enterTransitionSet = new TransitionSet();
        Transition transition = TransitionInflater.from(ctx).inflateTransition(android.R.transition.move);
        enterTransitionSet.addTransition(transition);
        enterTransitionSet.setDuration(MOVE_DEFAULT_TIME);
        enterTransitionSet.setStartDelay(FADE_DEFAULT_TIME);
        return enterTransitionSet;
    }

    // 3. Enter Transition for New Fragment
    public static Fade getEnterTranistion() {
        Fade enterFade = new Fade();
        enterFade.setStartDelay(MOVE_DEFAULT_TIME + FADE_DEFAULT_TIME);
        enterFade.setDuration(FADE_DEFAULT_TIME);
        return enterFade;
    }
}
