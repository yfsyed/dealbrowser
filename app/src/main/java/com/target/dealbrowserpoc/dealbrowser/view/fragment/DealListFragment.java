package com.target.dealbrowserpoc.dealbrowser.view.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.target.dealbrowserpoc.dealbrowser.BR;
import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.databinding.FragmentDealListBinding;
import com.target.dealbrowserpoc.dealbrowser.viewModel.DealsBrowserVM;

public class DealListFragment extends Fragment {

    public static final String TAG = DealListFragment.class.getSimpleName();

    private FragmentDealListBinding dealListBinding;
    private DealsBrowserVM vm;

    public DealListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        dealListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_deal_list, container, false);
        vm = ViewModelProviders.of(this).get(DealsBrowserVM.class);
        dealListBinding.setVariable(BR.actionCallBack, vm);
        return dealListBinding.getRoot();
    }
}