package com.target.dealbrowserpoc.dealbrowser.view.fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.databinding.FragmentDealDetailsBinding;
import com.target.dealbrowserpoc.dealbrowser.model.ProjectModel;
import com.target.dealbrowserpoc.dealbrowser.viewModel.DealsBrowserVM;

public class DealDetailsFragment extends Fragment {

    public static final String TAG = DealDetailsFragment.class.getSimpleName();

    private FragmentDealDetailsBinding eqDetailsBinding;
    private DealsBrowserVM vm;

    public DealDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        eqDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_deal_details, container, false);
        vm = ViewModelProviders.of(this).get(DealsBrowserVM.class);
        vm.init();
        eqDetailsBinding.setActionCallBack(vm);
        eqDetailsBinding.setProject(ProjectModel.getInstance());
        return eqDetailsBinding.getRoot();
    }

}
