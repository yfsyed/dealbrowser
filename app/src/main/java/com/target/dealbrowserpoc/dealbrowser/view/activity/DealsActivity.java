package com.target.dealbrowserpoc.dealbrowser.view.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;

import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.databinding.ActivityMainBinding;
import com.target.dealbrowserpoc.dealbrowser.factory.FragmentFactory;
import com.target.dealbrowserpoc.dealbrowser.model.DealItem;
import com.target.dealbrowserpoc.dealbrowser.model.ProjectModel;
import com.target.dealbrowserpoc.dealbrowser.utils.DealUtils;
import com.target.dealbrowserpoc.dealbrowser.utils.FragmentUtils;
import com.target.dealbrowserpoc.dealbrowser.viewModel.DealsBrowserVM;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class DealsActivity extends AppCompatActivity {

    private static final String EXTRA_SELECTED_DEAL = "com.target.dealbrowserpoc.dealbrowser.SELECTED_DEAL";
    private static final String EXTRA_DEAL_LIST = "com.target.dealbrowserpoc.dealbrowser.DEAL_LIST";

    private ActivityMainBinding dealsBinding;
    private DealsBrowserVM dealsBrowserVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dealsBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(dealsBinding.toolbar);
        //setup VM
        boolean isLandScape = getResources().getBoolean(R.bool.isLandscape);
        dealsBrowserVM = ViewModelProviders.of(this).get(DealsBrowserVM.class);
        dealsBrowserVM.setWidth(getWidth(isLandScape));
        dealsBrowserVM.setLandscape(isLandScape);
        dealsBinding.setActionCallBack(dealsBrowserVM);

        if (savedInstanceState == null) {
            FragmentFactory.showFragmentByType(new WeakReference<Context>(this),
                    R.id.fragment_container, FragmentFactory.Type.DEAL_LIST, true);
            dealsBrowserVM.fetchDeals(DealsActivity.this);
        } else {
            ArrayList<DealItem> dealsList = savedInstanceState.getParcelableArrayList(EXTRA_DEAL_LIST);
            if (dealsList != null) {
                ProjectModel.getInstance().setDeals(dealsList);
            }
            DealItem selectedEarthquake = savedInstanceState.getParcelable(EXTRA_SELECTED_DEAL);
            if (selectedEarthquake != null) {
                ProjectModel.getInstance().setSelectedDeal(selectedEarthquake);
            }
            if (FragmentUtils.getCount(getSupportFragmentManager()) > 1) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }
        if (isLandScape) {
            FragmentFactory.showFragmentByType(new WeakReference<Context>(this),
                    R.id.fragment_details_container,
                    FragmentFactory.Type.DEAL_DETAILS, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Don't create the menu for now
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
        //return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                performBackNavigation();
                return true;
            case R.id.action_settings:

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        performBackNavigation();
    }

    public void performBackNavigation() {
        FragmentManager fm = getSupportFragmentManager();
        if (FragmentUtils.getCount(fm) > 1) {
            fm.popBackStackImmediate();
        } else {
            DealUtils.closeOnTransition(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_DEAL_LIST,
                ProjectModel.getInstance().getDeals());
        outState.putParcelable(EXTRA_SELECTED_DEAL,
                ProjectModel.getInstance().getSelectedDeal());
    }

    public void showInListView(MenuItem menuItem) {
        dealsBrowserVM.showGridView(false);
    }

    public void showInGridView(MenuItem menuItem) {
        dealsBrowserVM.showGridView(true);
    }

    /**
     * Only get the width that corresponds to list fragment.
     *
     * @param isLandscape
     * @return
     */
    private int getWidth(boolean isLandscape) {
        if (isLandscape) {
            return (int) getResources().getDimension(R.dimen.list_landscape_width);
        }
        DisplayMetrics dm = new DisplayMetrics();
        Display display = getWindowManager().getDefaultDisplay();
        display.getMetrics(dm);
        return dm.widthPixels;
    }
}
