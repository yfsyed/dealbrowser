package com.target.dealbrowserpoc.dealbrowser.adapter;

import android.databinding.ViewDataBinding;

import com.target.dealbrowserpoc.dealbrowser.BR;
import com.target.dealbrowserpoc.dealbrowser.viewModel.DealsVM;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRecyclerAdapter<T extends ViewDataBinding,
        V extends DealsVM, K extends Object>
        extends DealsAdapter<T> {

    private List<K> mDataList = new ArrayList<>();
    private DealsVM<V> mCallbackVM;

    public AbstractRecyclerAdapter(int layoutId, DealsVM<V> callbackVM, List<K> data, int widthPx) {
        super(layoutId, widthPx);
        mDataList.clear();
        mDataList = data;
        mCallbackVM = callbackVM;
    }

    public AbstractRecyclerAdapter(int layoutId, DealsVM<V> callbackVM, List<K> data) {
        this(layoutId, callbackVM, data, 0);
    }

    @Override
    protected void bindItem(DealsViewHolder<T> holder, int position, List<Object> payloads) {
        holder.binding.setVariable(BR.actionCallBack, mCallbackVM.getVM());
        holder.binding.setVariable(BR.data, getItem(position));
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    protected K getItem(int position) {
        return mDataList.get(position);
    }

}
