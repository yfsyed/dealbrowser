package com.target.dealbrowserpoc.dealbrowser.adapter;

import android.databinding.OnRebindCallback;
import android.databinding.ViewDataBinding;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

public abstract class BaseDealsAdapter<T extends ViewDataBinding> extends
        RecyclerView.Adapter<DealsViewHolder<T>> {

    private static final Object Doorways_Data = new Object();
    private int widthPx = 0;

    public BaseDealsAdapter() {
    }

    public BaseDealsAdapter(int width) {
        widthPx = width;
    }

    @Nullable
    private RecyclerView mRecyclerView;

    private final OnRebindCallback onRebindCallback = new OnRebindCallback() {
        @Override
        public boolean onPreBind(ViewDataBinding binding) {
            if (mRecyclerView == null || mRecyclerView.isComputingLayout()) {
                return true;
            }
            int childPosition = mRecyclerView.getChildAdapterPosition(binding.getRoot());
            if (childPosition == RecyclerView.NO_POSITION) {
                return true;
            }
            notifyItemChanged(childPosition, Doorways_Data);
            return false;
        }
    };

    @Override
    @CallSuper
    public DealsViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        DealsViewHolder<T> viewHolder = DealsViewHolder.create(parent, viewType);
        viewHolder.binding.addOnRebindCallback(onRebindCallback);
        if (widthPx != 0) {
            viewHolder.binding.getRoot().getLayoutParams().width = widthPx;
            viewHolder.binding.getRoot().getLayoutParams().height = (int) (widthPx * 1.5);
        }
        return viewHolder;
    }

    @Override
    public final void onBindViewHolder(DealsViewHolder<T> holder, int position,
                                       List<Object> data) {
        if (data.isEmpty() || hasNonDataBindingInvalidate(data)) {
            bindItem(holder, position, data);
        }
        holder.binding.executePendingBindings();
    }

    private boolean hasNonDataBindingInvalidate(List<Object> dataSet) {
        for (Object data : dataSet) {
            if (data != Doorways_Data) {
                return true;
            }
        }
        return false;
    }

    @Override
    public final void onBindViewHolder(DealsViewHolder<T> holder, int position) {
        throw new IllegalArgumentException("just overridden to make final.");
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        mRecyclerView = null;
    }

    @Override
    public final int getItemViewType(int position) {
        return getItemLayoutId(position);
    }

    protected abstract void bindItem(DealsViewHolder<T> holder, int position,
                                     List<Object> payloads);

    @LayoutRes
    abstract public int getItemLayoutId(int position);

}
