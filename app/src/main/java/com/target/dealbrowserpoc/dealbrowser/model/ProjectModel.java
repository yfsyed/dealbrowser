package com.target.dealbrowserpoc.dealbrowser.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProjectModel extends BaseObservable {

    private DealItem selectedDeal;
    private ArrayList<DealItem> dealList;
    private static ProjectModel sInstance;
    private Map<Integer, Bitmap> imageMap;

    static {
        sInstance = new ProjectModel();
    }

    private ProjectModel() {
        imageMap = new HashMap<>();
    }

    public static ProjectModel getInstance() {
        return sInstance;
    }

    public ArrayList<DealItem> getDeals() {
        if (dealList == null) {
            dealList = new ArrayList<>();
        }
        return dealList;
    }

    public void setDeals(ArrayList<DealItem> earthquakes) {
        dealList = earthquakes;
        if (dealList != null && dealList.size() > 0) {
            selectedDeal = getDeals().get(0);
        }
        notifyChange();
    }

    public void setSelectedDeal(DealItem deal) {
        imageMap.clear();
        selectedDeal = deal;
        notifyChange();
    }

    @Bindable
    public DealItem getSelectedDeal() {
        return selectedDeal;
    }

    public synchronized void add(int index, Bitmap bmp) {
        imageMap.put(index, bmp);
    }

    public Bitmap getImage(int index) {
        return imageMap.get(index);
    }
}
