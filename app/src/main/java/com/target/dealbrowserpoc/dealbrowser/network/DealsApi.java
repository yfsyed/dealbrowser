package com.target.dealbrowserpoc.dealbrowser.network;

import com.target.dealbrowserpoc.dealbrowser.model.DealList;

import retrofit2.http.GET;
import rx.Observable;

public interface DealsApi {

    @GET("/api/deals")
    Observable<DealList> getDealList();
}
