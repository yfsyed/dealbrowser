package com.target.dealbrowserpoc.dealbrowser.model;

import java.util.ArrayList;

public class DealList {

    private String _id;
    private String type;
    private ArrayList<DealItem> data;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<DealItem> getData() {
        return data;
    }

    public void setData(ArrayList<DealItem> data) {
        this.data = data;
    }

}
