package com.target.dealbrowserpoc.dealbrowser.viewModel;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.target.dealbrowserpoc.dealbrowser.DealBrowserApplication;
import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.binding.BindableBoolean;
import com.target.dealbrowserpoc.dealbrowser.binding.BindableInt;
import com.target.dealbrowserpoc.dealbrowser.factory.AdapterFactory;
import com.target.dealbrowserpoc.dealbrowser.factory.FragmentFactory;
import com.target.dealbrowserpoc.dealbrowser.model.DealItem;
import com.target.dealbrowserpoc.dealbrowser.model.ProjectModel;
import com.target.dealbrowserpoc.dealbrowser.network.RxUtil;
import com.target.dealbrowserpoc.dealbrowser.utils.DealUtils;

import java.lang.ref.WeakReference;
import java.util.List;

public class DealsBrowserVM extends DealsVM {

    public static BindableInt progress = new BindableInt();
    public static BindableBoolean isLandscape = new BindableBoolean();

    public int screenWidth;

    public void setLandscape(boolean landscape) {
        isLandscape.set(landscape);
    }

    public List<DealItem> getDealsList() {
        return ProjectModel.getInstance().getDeals();
    }

    private DealItem currentSelection;

    public void init() {
        if (currentSelection == null) {
            currentSelection = ProjectModel.getInstance().getSelectedDeal();
        }
    }

    public void setWidth(int width) {
        screenWidth = width;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public void onDealSelected(View view, DealItem dealItem) {
        if (progress.get() == 1)
            return;

        if (dealItem != currentSelection) {
            if (currentSelection != null) currentSelection.setSelected(false);
            if (dealItem != null) dealItem.setSelected(true);
            currentSelection = dealItem;
            ProjectModel.getInstance().setSelectedDeal(dealItem);
            showDetailsFragment(view.getContext());
        }
    }

    public void fetchDeals(Context ctx) {
        if (progress.get() == 0) {
            RxUtil.fetchDealsList(ctx, progress, getNetworkAdapter());
        }
    }

    public AdapterFactory.Type getDealsListType() {
        if (showGrid.get()) {
            return AdapterFactory.Type.DEALS_GRID;
        } else {
            return AdapterFactory.Type.DEALS_LIST;
        }
    }

    @Override
    public ViewModel getVM() {
        return this;
    }

    private void showDetailsFragment(Context ctx) {
        if (!isLandscape.get()) {
            WeakReference<Context> ctxReference = new WeakReference<>(ctx);
            FragmentFactory.showFragmentByType(ctxReference,
                    R.id.fragment_container, FragmentFactory.Type.DEAL_DETAILS, true);
        }
    }

    public String getPrice(String price) {
        if (TextUtils.isEmpty(price)) {
            return DealUtils.getText(R.string.too_low_to_show);
        }
        return price;
    }

    public void onRefresh() {
        fetchDeals(DealBrowserApplication.getInstance());
    }

    public void showGridView(boolean grid) {
        showGrid.set(grid);
    }

    public static BindableBoolean showGrid = new BindableBoolean();
}
