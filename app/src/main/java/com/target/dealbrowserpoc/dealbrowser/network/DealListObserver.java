package com.target.dealbrowserpoc.dealbrowser.network;

import android.content.Context;

import com.target.dealbrowserpoc.dealbrowser.binding.BindableInt;
import com.target.dealbrowserpoc.dealbrowser.model.DealItem;
import com.target.dealbrowserpoc.dealbrowser.model.DealList;
import com.target.dealbrowserpoc.dealbrowser.model.ProjectModel;

import java.util.ArrayList;

public class DealListObserver extends BaseResponseObserver<DealList> {

    public DealListObserver(Context ctx, BindableInt progress) {
        super(ctx, progress);
    }

    @Override
    public void onNext(DealList earthQuakeList) {
        ProjectModel.getInstance().setDeals(
                earthQuakeList != null ? earthQuakeList.getData() : new ArrayList<DealItem>());
    }
}