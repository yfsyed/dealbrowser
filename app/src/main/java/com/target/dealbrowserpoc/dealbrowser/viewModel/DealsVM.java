package com.target.dealbrowserpoc.dealbrowser.viewModel;

import android.arch.lifecycle.ViewModel;

import com.target.dealbrowserpoc.dealbrowser.DealBrowserApplication;
import com.target.dealbrowserpoc.dealbrowser.network.DealsNetworkAdapter;

public abstract class DealsVM<T extends ViewModel> extends ViewModel {
    public final T viewModel;

    public DealsVM() {
        this.viewModel = getVM();
    }

    public abstract T getVM();

    public DealsNetworkAdapter getNetworkAdapter() {
        return DealBrowserApplication.getInstance().getNetworkAdapter();
    }
}
