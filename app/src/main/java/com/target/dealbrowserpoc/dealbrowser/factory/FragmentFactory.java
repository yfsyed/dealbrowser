package com.target.dealbrowserpoc.dealbrowser.factory;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.target.dealbrowserpoc.dealbrowser.utils.FragmentUtils;
import com.target.dealbrowserpoc.dealbrowser.view.fragment.DealDetailsFragment;
import com.target.dealbrowserpoc.dealbrowser.view.fragment.DealListFragment;

import java.lang.ref.WeakReference;

public class FragmentFactory {

    public enum Type {
        DEAL_LIST(DealListFragment.TAG, DealListFragment.class),
        DEAL_DETAILS(DealDetailsFragment.TAG, DealDetailsFragment.class);

        String fragmentName;
        Class className;

        Type(String name, Class className) {
            this.className = className;
            this.fragmentName = name;
        }

        public Class getClassName() {
            return className;
        }

        public String getFragmentName() {
            return fragmentName;
        }
    }

    public static void showFragmentByType(WeakReference<Context> ctx, int layoutId, Type type, boolean backstack) {
        if (type == null)
            return;
        FragmentUtils.showFragmentWithType(ctx, type, layoutId, backstack);
    }

    public static Fragment getFragmentClass(Type type) throws
            InstantiationException, IllegalAccessException {
        return (Fragment) type.getClassName().newInstance();
    }
}
