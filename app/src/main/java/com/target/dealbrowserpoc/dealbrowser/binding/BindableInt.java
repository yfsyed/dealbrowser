package com.target.dealbrowserpoc.dealbrowser.binding;

import android.databinding.BaseObservable;

public class BindableInt extends BaseObservable {
    private Integer value = 0;

    public Integer get() {
        return value;
    }

    public void set(Integer num) {
        if (!num.equals(value)) {
            value = num;
            notifyChange();
        }
    }
}
