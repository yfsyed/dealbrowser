package com.target.dealbrowserpoc.dealbrowser.binding;

import android.databinding.BaseObservable;

public class BindableBoolean extends BaseObservable {
    private Boolean value = false;

    public Boolean get() {
        return value;
    }

    public void set(Boolean num) {
        if (!num.equals(value)) {
            value = num;
            notifyChange();
        }
    }
}
