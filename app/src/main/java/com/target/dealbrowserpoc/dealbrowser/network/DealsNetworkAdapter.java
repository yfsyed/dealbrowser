package com.target.dealbrowserpoc.dealbrowser.network;

import com.target.dealbrowserpoc.dealbrowser.BuildConfig;
import com.target.dealbrowserpoc.dealbrowser.model.DealList;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class DealsNetworkAdapter {

    private DealsApi mApi;

    public DealsNetworkAdapter() {
        Retrofit mRequest = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
        mApi = mRequest.create(DealsApi.class);
    }

    public Observable<DealList> getDealList() {
        return mApi.getDealList();
    }
}
