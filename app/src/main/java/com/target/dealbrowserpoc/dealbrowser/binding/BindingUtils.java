package com.target.dealbrowserpoc.dealbrowser.binding;

import android.databinding.BindingAdapter;
import android.support.annotation.DrawableRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.adapter.DealsAdapter;
import com.target.dealbrowserpoc.dealbrowser.factory.AdapterFactory;
import com.target.dealbrowserpoc.dealbrowser.model.CardItem;
import com.target.dealbrowserpoc.dealbrowser.model.DealItem;
import com.target.dealbrowserpoc.dealbrowser.model.ProjectModel;
import com.target.dealbrowserpoc.dealbrowser.viewModel.DealsBrowserVM;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.VISIBLE;

public class BindingUtils {

    public static final int DEAL_SPAN = 2;

    @BindingAdapter("app:visibility")
    public static void setVisibility(View view, CardItem cardItem) {
        view.setSelected(cardItem.isSelected());
    }

    @BindingAdapter("app:text")
    public static void showView(TextView view, int resId) {
        if (resId > 0) view.setText(resId);
    }

    @BindingAdapter({"app:updateOnChange", "app:empty_view", "app:viewModel", "app:type", "app:mode"})
    public static void updateDeals(RecyclerView recyclerView,
                                   BindableInt progress, TextView emptyView,
                                   DealsBrowserVM vm, AdapterFactory.Type type, boolean isGrid) {
        if (progress.get() == 1)
            return;


        emptyView.setVisibility(View.GONE);
        List<DealItem> list = vm.getDealsList();
        if (list == null || list.isEmpty()) {
            emptyView.setVisibility(VISIBLE);
            list = new ArrayList<>();
        }
        DealsAdapter adapter;
        if (isGrid) {
            recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), DEAL_SPAN));
            int cellWidth = vm.getScreenWidth() / DEAL_SPAN;
            adapter = AdapterFactory.getAdapterInstance(type, vm, list, cellWidth);
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            adapter = AdapterFactory.getAdapterInstance(type, vm, list);
        }

        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter({"app:binder", "app:resId", "app:text"})
    public static void setText(TextView view, ProjectModel data, String resString, String value) {
        view.setText(!TextUtils.isEmpty(value) ? String.format(resString, value) : "");
    }

    @BindingAdapter({"app:binder", "app:text"})
    public static void setText(TextView view, ProjectModel data, String value) {
        view.setText(value);
    }

    @BindingAdapter({"app:binder", "app:loadImage"})
    public static void setImage(ImageView view, ProjectModel data, @DrawableRes int resId) {
        if (resId <= 0) {
            view.setImageResource(R.drawable.ic_launcher);
            return;
        }
        Picasso.with(view.getContext()).load(resId).into(view);
    }

    @BindingAdapter({"app:binder", "app:loadImage"})
    public static void setImage(ImageView view, ProjectModel data, String url) {
        if (TextUtils.isEmpty(url)) {
            view.setImageResource(R.drawable.ic_launcher);
            return;
        }
        Picasso.with(view.getContext())
                .load(url)
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(view);
    }

    @BindingAdapter("app:loadImage")
    public static void setImage(ImageView view, @DrawableRes int resId) {
        if (resId <= 0) {
            view.setImageResource(R.drawable.ic_launcher);
            return;
        }
        Picasso.with(view.getContext()).load(resId).into(view);
    }

    @BindingAdapter({"app:loadImage"})
    public static synchronized void setImage(ImageView view, String url) {
        if (TextUtils.isEmpty(url)) {
            view.setImageResource(R.drawable.ic_launcher);
            return;
        }
        Picasso.with(view.getContext())
                .load(url)
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(view);

    }

    @BindingAdapter({"app:binder", "app:resId", "app:text"})
    public static void setSpannableText(TextView view, ProjectModel data, String resString, String value) {
        if (TextUtils.isEmpty(resString) || TextUtils.isEmpty(value)) {
            return;
        }
        String txt = String.format(resString, value);
        SpannableString text = new SpannableString(txt);
        int normalCount = txt.length() - value.length();
        text.setSpan(new StrikethroughSpan(), normalCount, txt.length(), 0);
        view.setText(text, TextView.BufferType.SPANNABLE);
    }

    @BindingAdapter({"app:binder", "app:viewModel"})
    public static void setRefresh(SwipeRefreshLayout view, BindableInt progress, DealsBrowserVM vm) {
        if (vm == null) return;

        view.setOnRefreshListener(() -> vm.onRefresh());
        view.setRefreshing(progress.get() == 1);
        view.setColorSchemeResources(
                R.color.refresh_progress_1,
                R.color.refresh_progress_2,
                R.color.refresh_progress_3);
    }
}
