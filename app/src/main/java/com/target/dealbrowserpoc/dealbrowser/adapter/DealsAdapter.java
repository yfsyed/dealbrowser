package com.target.dealbrowserpoc.dealbrowser.adapter;

import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;

public abstract class DealsAdapter<T extends ViewDataBinding> extends BaseDealsAdapter<T> {

    @LayoutRes
    private final int mLayoutId;

    public DealsAdapter(@LayoutRes int layoutId, int widthPx) {
        super(widthPx);
        mLayoutId = layoutId;
    }

    public DealsAdapter(@LayoutRes int layoutId) {
        mLayoutId = layoutId;
    }

    @Override
    public int getItemLayoutId(int position) {
        return mLayoutId;
    }

}
