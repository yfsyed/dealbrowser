package com.target.dealbrowserpoc.dealbrowser.network;

import android.content.Context;

import com.target.dealbrowserpoc.dealbrowser.R;
import com.target.dealbrowserpoc.dealbrowser.binding.BindableInt;
import com.target.dealbrowserpoc.dealbrowser.utils.DealUtils;

import java.lang.ref.WeakReference;

import rx.Observer;

public class BaseResponseObserver<T extends Object> implements Observer<T> {

    protected WeakReference<Context> weakContext;
    private BindableInt progress;

    public BaseResponseObserver(Context ctx, BindableInt progress) {
        weakContext = new WeakReference<>(ctx);
        this.progress = progress;
    }

    @Override
    public void onCompleted() {
        progress.set(0);
    }

    @Override
    public void onError(Throwable e) {
        progress.set(0);
        Context ctx = weakContext.get();
        if (ctx != null) {
            DealUtils.showInfoMessage(ctx, R.string.unable_to_fetch_data);
        }
    }

    @Override
    public void onNext(T t) {

    }
}
