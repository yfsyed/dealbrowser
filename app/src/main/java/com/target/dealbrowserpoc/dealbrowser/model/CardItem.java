package com.target.dealbrowserpoc.dealbrowser.model;

import android.databinding.BaseObservable;

public class CardItem extends BaseObservable {
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
