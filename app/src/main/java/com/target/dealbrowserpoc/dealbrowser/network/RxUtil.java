package com.target.dealbrowserpoc.dealbrowser.network;

import android.content.Context;

import com.target.dealbrowserpoc.dealbrowser.binding.BindableInt;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RxUtil {
    /**
     * @param ctx            Activity context to update error responses.
     * @param progress       Bindable int that helps in displaying progress update.
     * @param networkAdapter Network adapter to talk to retrofit interface.
     */
    public static void fetchDealsList(
            Context ctx,
            BindableInt progress,
            DealsNetworkAdapter networkAdapter) {
        progress.set(1);
        networkAdapter.getDealList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DealListObserver(ctx, progress));
    }
}
