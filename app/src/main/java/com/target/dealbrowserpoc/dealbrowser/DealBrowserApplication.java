package com.target.dealbrowserpoc.dealbrowser;

import android.app.Application;

import com.target.dealbrowserpoc.dealbrowser.network.DealsNetworkAdapter;

public class DealBrowserApplication extends Application {
    private static DealBrowserApplication sInstance;
    private DealsNetworkAdapter networkAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        networkAdapter = new DealsNetworkAdapter();
    }

    public static DealBrowserApplication getInstance() {
        return sInstance;
    }

    public DealsNetworkAdapter getNetworkAdapter() {
        return networkAdapter;
    }
}