package com.target.dealbrowserpoc.dealbrowser.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DealItem extends CardItem implements Parcelable {

    @SerializedName("_id")
    private String dealId;
    private String aisle;
    private String description;
    private String guid;
    private String image;
    private int index;
    private String price;
    private String salePrice;
    private String title;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String id) {
        this.dealId = id;
    }

    public String getAisle() {
        return aisle;
    }

    public void setAisle(String aisle) {
        this.aisle = aisle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.index);
        dest.writeString(this.dealId);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.price);
        dest.writeString(this.salePrice);
        dest.writeString(this.image);
        dest.writeString(this.aisle);
    }

    protected DealItem(Parcel in) {
        this.index = in.readInt();
        this.dealId = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.price = in.readString();
        this.salePrice = in.readString();
        this.image = in.readString();
        this.aisle = in.readString();
    }

    public static final Parcelable.Creator<DealItem> CREATOR = new Parcelable.Creator<DealItem>() {
        @Override
        public DealItem createFromParcel(Parcel source) {
            return new DealItem(source);
        }

        @Override
        public DealItem[] newArray(int size) {
            return new DealItem[size];
        }
    };
}